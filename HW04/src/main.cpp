/**
 *
 *	Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 *	Student Number: 111044016
 *	CSE463 - Introduction to Computer Vision
 *	Homework 04 - Background Substraction and Object Tracking
 *
 */

#include <iostream>
#include <cmath>
#include <unistd.h>

#include <opencv2/opencv.hpp>


// EXIT CODES
//static const int EXIT_SUCCESS = 0;
static const int EXIT_FAIL_TO_OPEN_VIDEO = -10;
static const int EXIT_FAIL_TO_READ_FRAME = -15;


// KEY CODES
static const char KEYCODE_ESCAPE = 27;
static const char KEYCODE_SPACE = 32;

// DEVICE ID FOR WEB CAM(S)
static const int VIDEO_DEVICE_ID = 0;

// WINDOW NAME(S)
static const std::string WINDOW_NAME_VIDEO_FRAME = "VIDEO FRAME";
static const std::string WINDOW_NAME_FOREGROUND_IMAGE = "FOREGROUND IMAGE";
static const std::string WINDOW_NAME_FOREGROUND_MASK = "FOREGROUND MASK";
static const std::string WINDOW_NAME_BACKGROUND_IMAGE = "BACKGROUND IMAGE";

// SOME DEFAULTS
static const std::string DEFAULT_VIDEO_FILE("traffic.mp4");
//static const std::string DEFAULT_VEHICLE_CLASSIFIER_FILE("../classifier/vehicle.xml");
static const std::string DEFAULT_VEHICLE_CLASSIFIER_FILE("vehicle.xml");
static const cv::Rect DEFAULT_TEXT_BOX(2, 2, 200, 30);


// BACKGROUND SUBSTRACTION SETTINGS
static const int BGS_HISTORY_LENGTH = 1000;
static const float BGS_THRESHOLD = 16;
static const bool BGS_DETECT_SHADOWS = false;

// CLASSIFIER SETTINGS
static const cv::Size CLASS_MIN_SIZE(100, 100);
static const cv::Size CLASS_MAX_SIZE(300, 300);


void printHelpMessage(const char *argv0);

/**
 * Pauses execution until given delay time passes.
 * Returns keycode of pressed key
 *
 * Calls opencv library's cv::waitKey(int) internally so this function can be used
 * for invoking highgui operations.
 *
 * Giving zero (0) as delay parameter makes execution to wait until  key press
 *
 * @param delay length of wait time in terms of milliseconds (ms), default is 50 ms
 * @return Keycode of pressed key
 */
static inline const int waitAnyKey(const int delay = 50);

/**
 * Pauses execution until given delay time passes.
 * Returns true if user presses selected key on Keyboard while delay time didn't end
 *
 * Calls opencv library's cv::waitKey(int) internally so this function can be used
 * for invoking highgui operations.
 *
 * Giving zero (0) as delay parameter makes execution to wait until  key press
 *
 * @param key ASCII keycode for selected key, default is 27  (ESC key code)
 * @param delay length of wait time in terms of milliseconds (ms), default is 50 ms
 * @return true if user pressed selected key while waiting. false otherwise
 */
static inline const bool waitForKey(const char key = KEYCODE_ESCAPE, const int delay = 50);

void putPoint(cv::Mat &im, const cv::Point point);

void putRectangle(cv::Mat &im, const cv::Rect rectangle, const int thickness = 1);

void putText(cv::Mat &im, const std::string label, const cv::Rect rectangle,
             const int thickness = 1, const cv::Scalar &color = CV_RGB(255, 0, 0));

void putFrameNumber(cv::Mat &im, const uint64_t number, const cv::Rect rectangle,
                    const int thickness = 1, const cv::Scalar &color = CV_RGB(255, 0, 0));

void putFrameNumber(cv::Mat &im, cv::VideoCapture &capture, const cv::Rect rectangle,
                    const int thickness = 1, const cv::Scalar &color = CV_RGB(255, 0, 0));

void createWindow(const std::string &windowName, const int width = 480, const int height = 270,
                  const int xpos = 0, const int ypos = 0);

void extractForegroundObjects(cv::Mat &im, const cv::Mat &foregroundMask);

int main(int argc, char **argv) {

    std::string videoFileName;
    if (argc > 1) {
        char tmp;
        while ((tmp = (char) getopt(argc, argv, "fh")) != -1) {
            switch (tmp) {
                case 'h':
                    printHelpMessage(argv[0]);
                    return (EXIT_SUCCESS);
                    break;
                case 'f':
                    videoFileName.assign(argv[optind]);
                    break;
                default:
                    std::cerr << "[ERR] Illegal Argument Found!!!" << std::endl;
                    break;
            }
        }
    } else {
        videoFileName = DEFAULT_VIDEO_FILE;
    }


    std::cerr << "[INF] Video File:" << videoFileName << std::endl;

    cv::VideoCapture capture(videoFileName);

    if (!capture.isOpened()) {
        std::cerr << "[ERR] Failed to open Video File!!! Exiting..." << std::endl;
        capture.release();
        return (EXIT_FAIL_TO_OPEN_VIDEO);
    }

    cv::CascadeClassifier vehicleClassifier(DEFAULT_VEHICLE_CLASSIFIER_FILE);

    cv::BackgroundSubtractorMOG2 bgSubs(BGS_HISTORY_LENGTH, BGS_THRESHOLD, BGS_DETECT_SHADOWS);

    cv::Mat videoFrame, bgFrame, fgMaskFrame;

    createWindow(WINDOW_NAME_VIDEO_FRAME, 480, 270, 10);
    createWindow(WINDOW_NAME_FOREGROUND_IMAGE, 480, 270, 510);
    createWindow(WINDOW_NAME_BACKGROUND_IMAGE, 480, 270, 10, 340);
    createWindow(WINDOW_NAME_FOREGROUND_MASK, 480, 270, 510, 340);

    waitAnyKey(30);

    std::cerr << "[INF] Begin processing" << std::endl;

    uint64_t numFrames = 0;
    int keycode = 0;
    while (true) {
        if (!capture.read(videoFrame)) {
            std::cerr << "[ERR] Failed to read next frame!!! Exiting..." << std::endl;
            return (EXIT_FAIL_TO_READ_FRAME);
        }
        ++numFrames;

        bgSubs.operator()(videoFrame, fgMaskFrame);
        bgSubs.getBackgroundImage(bgFrame);


        cv::Mat fgFrame(1, 1, 0.0);
        videoFrame.copyTo(fgFrame, fgMaskFrame);
        //videoFrame.copyTo(fgFrame);

        /*
        std::vector<std::vector<cv::Point>> contours;
        cv::findContours(fgMaskFrame.clone(), contours, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);

        for(int i=0; i<contours.size(); ++i)
        {
            if(contours[i].size() > 200)
            {
                cv::Rect roi = cv::boundingRect(contours[i]);
                drawContours(fgFrame, contours, i, cv::Scalar(0,0,255));
                rectangle(fgFrame, roi, cv::Scalar(0,255,0));
            }
        }*/

        std::vector<cv::Rect> vehicles;

        vehicleClassifier.detectMultiScale(videoFrame, vehicles, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, CLASS_MIN_SIZE, CLASS_MAX_SIZE);
        //vehicleClassifier.detectMultiScale(fgFrame, vehicles, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, CLASS_MIN_SIZE, CLASS_MAX_SIZE);

        std::cerr << "[INF] Total of " << vehicles.size() << " vehicles detected" << std::endl;

        if (vehicles.size() > 0)
            for(size_t i = 0; i < vehicles.size(); ++i){
                putRectangle(videoFrame, vehicles[i]);
            }

        std::cerr << "[INF] Total of " << vehicles.size() << " vehicles marked" << std::endl;

        putFrameNumber(videoFrame, numFrames, DEFAULT_TEXT_BOX, 2);

        cv::imshow(WINDOW_NAME_VIDEO_FRAME, videoFrame);
        cv::imshow(WINDOW_NAME_BACKGROUND_IMAGE, bgFrame);
        cv::imshow(WINDOW_NAME_FOREGROUND_IMAGE, fgFrame);
        cv::imshow(WINDOW_NAME_FOREGROUND_MASK, fgMaskFrame);

        std::cerr << "[INF] Frame update on windows" << std::endl;


        keycode = waitAnyKey();
        switch (keycode) {
            case KEYCODE_ESCAPE:
                cv::destroyAllWindows();
                capture.release();
                std::cerr << "[INF] Successful exit." << std::endl;
                return (EXIT_SUCCESS);
                break;
            case KEYCODE_SPACE:
                std::cerr << "[INF] Saving Background Image" << std::endl;
                cv::imwrite("background.png", bgFrame);
                break;
            default:
                //std::cerr << "[INF] Keypress Detected. Keycode: " << keycode << std::endl;
                break;
        }
    } // end while
} // end main

void printHelpMessage(const char *argv0) {
    std::cerr << argv0 << " [-f|-h]" << std::endl
    << "\t-f specify a video file for testing." << std::endl
    << "\t-h shows this help meesage" << std::endl
    << std::endl
    << "Running program with no parameters means program " << std::endl
    << "will try to open \"traffic.mp4\" file on working directory" << std::endl;
}


static inline const int waitAnyKey(const int delay) {
    return (cv::waitKey(delay) % 256);
}

static inline const bool waitForKey(const char key, const int delay) {
    return ((cv::waitKey(delay) % 256) == key);
}

cv::RNG rng(123424);


void putPoint(cv::Mat &im, const cv::Point point) {
    int thickness = -1;
    int lineType = 8;
    cv::Scalar color(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
    cv::circle(im, point, 1, color, thickness, lineType);
    //std::cout <<"point: " << point << std::endl;
}

void putRectangle(cv::Mat &im, const cv::Rect rectangle, const int thickness) {
    int lineType = 8;
    cv::Scalar color(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
    cv::rectangle(im, rectangle, color, thickness, lineType, 0);
}

void putText(cv::Mat &im, const std::string label, const cv::Rect rectangle, const int thickness,
             const cv::Scalar &color) {
    int fontface = cv::FONT_HERSHEY_SIMPLEX;
    double scale = 1;
    int baseline = 0;

    cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);

    cv::Point pt(rectangle.x + ((rectangle.width - text.width) / 2),
                 rectangle.y + ((rectangle.height + text.height) / 2));

    cv::putText(im, label, pt, fontface, scale, color, thickness, 8);
}


void putFrameNumber(cv::Mat &im, const uint64_t number, const cv::Rect rectangle,
                    const int thickness, const cv::Scalar &color) {
    std::stringstream ss;
    ss << number;
    putText(im, ss.str(), rectangle, thickness, color);
}

void putFrameNumber(cv::Mat &im, cv::VideoCapture &capture, const cv::Rect rectangle,
                    const int thickness, const cv::Scalar &color) {
    std::stringstream ss;
    ss << capture.get(CV_CAP_PROP_POS_FRAMES);
    putText(im, ss.str(), rectangle, thickness, color);
}


void createWindow(const std::string &windowName, const int width, const int height,
                  const int xpos, const int ypos) {
    cv::namedWindow(windowName, 0);
    cv::resizeWindow(windowName, width, height);
    cv::moveWindow(windowName, xpos, ypos);
}