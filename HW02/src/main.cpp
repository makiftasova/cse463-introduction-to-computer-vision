/**
 *
 *	Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 *	Student Number: 111044016
 *	CSE463 - Introduction to Computer Vision
 *	Homework 01 - Thresholding
 *
 */

#include <iostream>
#include <cmath>
#include <unistd.h>


#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// STRUCTS
struct Marker {
    cv::Rect area;
    unsigned int number;
};


// EXIT CODES
//static const int EXIT_SUCCESS = 0;
static const int EXIT_FAIL_TO_OPEN_VIDEO_DEVICE = -10;


// KEY CODES
static const char KEYCODE_ESCAPE = 27;
static const char KEYCODE_SPACE = 32;

// DEVICE ID FOR WEB CAM(S)
static const int VIDEO_DEVICE_ID = 0;


// CONTOUR HIERARCHY DEFINITIONS
static const int CH_IND_NEXT = 0;  // array index
static const int CH_IND_PREV = 1; // array index
static const int CH_IND_FIRST_CHILD = 2; // array index
static const int CH_IND_PARENT = 3; // array index
static const int CH_NO_CONTOUR = -1; // simply means null pointer for hierarchy array

// RECT OFFSETS
static const int XW_OFFSET = 20;
static const int YH_OFFSET = 18;


// WINDOW NAME(S)
static const std::string WINDOW_NAME_VIDEO_FEED = "VIDEO FEED";
static const std::string WINDOW_NAME_MARKER = "MARKER";
static const std::string WINDOW_NAME_BW_IMG = "BW IMG";

// ERROR VALUES FOR FUNCTIONS
static const cv::Rect NO_RECT_FOUND(0, 0, 0, 0);


void printHelpMessage(const char *argv0);


/**
 * Pauses execution until given delay time passes.
 * Returns true if user presses selected key on Keyboard while delay time didn't end
 *
 * Calls opencv library's cv::waitKey(int) internally so this function can be used
 * for invoking highgui operations.
 *
 * Giving zero (0) as delay parameter makes execution to wait until  key press
 *
 * @param key ASCII keycode for selected key, default is 27  (ESC key code)
 * @param delay length of wait time in terms of milliseconds (ms), default is 50 ms
 * @return true if user pressed selected key while waiting. false otherwise
 */
static inline const bool waitForKey(const char key = KEYCODE_ESCAPE, const int delay = 50);


double pointDistanceToLine(cv::Point point, cv::Point lineStart, cv::Point lineEnd);

std::vector<cv::Point> DouglasPeuckerApproximation(std::vector<cv::Point> contours, double epsilon, bool closed = true);

/**
 * Helper function to find a cosine of angle between vectors
 * from pt0->pt1 and pt0->pt2
 */
static double vectorAngle(const cv::Point &pt1, const cv::Point &pt2, const cv::Point &pt0);

void putPoint(cv::Mat &im, const cv::Point point);

void putRectangle(cv::Mat &im, const cv::Rect rectangle, const int thickness = 1);

void putText(cv::Mat &im, const std::string label, const cv::Rect rectangle,
             const int thickness = 1, const cv::Scalar &color = CV_RGB(255, 0, 0));


std::vector<Marker> findMarkers(cv::Mat &bwImg);

std::string getMarkerText(const unsigned int markerNumber);


static bool workOnRealMarkers = false;

int main(int argc, char **argv) {

    if (argc > 1) {
        char tmp;
        while ((tmp = (char) getopt(argc, argv, "hm")) != -1) {
            switch (tmp) {
                case 'h':
                    printHelpMessage(argv[0]);
                    return (EXIT_SUCCESS);
                    break;
                case 'm':
                    workOnRealMarkers = true;
                    std::cerr << "Real Marker Mode Enabled" << std::endl;
                    break;
                default:
                    workOnRealMarkers = false;
                    break;
            }
        }
    } else {
        workOnRealMarkers = false;
    }


    cv::VideoCapture webCamStream(VIDEO_DEVICE_ID);

    if (!webCamStream.isOpened()) {
        std::cerr << "[ERR] Failed to open video device! device ID: " << VIDEO_DEVICE_ID << std::endl;
        return (EXIT_FAIL_TO_OPEN_VIDEO_DEVICE);
    }

    while (1) {

        cv::Mat srcFrame;
        webCamStream >> srcFrame;


        cv::Mat bwImg; // black white image
        cv::cvtColor(srcFrame, bwImg, CV_BGR2GRAY);


        cv::blur(bwImg, bwImg, cv::Size(3, 3));

        cv::Canny(bwImg, bwImg, 0, 255);

        cv::imshow(WINDOW_NAME_BW_IMG, bwImg);

        std::vector<Marker> markers = findMarkers(bwImg);

        cv::Mat dstFrame = srcFrame.clone();

        for (int i = 0; i < markers.size(); ++i) {
            putRectangle(dstFrame, markers[i].area, 8);
            cv::Mat roi = bwImg(markers[i].area);
            putText(dstFrame, getMarkerText(markers[i].number), markers[i].area);
        }


        cv::imshow(WINDOW_NAME_VIDEO_FEED, dstFrame);

#ifdef DEBUG_ON
        if (markers.size() >= 1) {
            cv::imshow(WINDOW_NAME_MARKER, srcFrame(markers[0]));
            std::cout << "rect: " << markers[0] << std::endl;
        }
#endif

        if (waitForKey()) {
            webCamStream.release();
            cv::destroyAllWindows();
            std::cout << "[INF] Successful exit." << std::endl;
            return (EXIT_SUCCESS);
        }
    } // end while
} // end main

void printHelpMessage(const char *argv0) {
    std::cerr << argv0 << " [-h|-m]" << std::endl
    << "\t-h shows this help meesage" << std::endl
    << "\t-m runs program in real marker mode." << std::endl
    << std::endl
    << "Running program with no parameters means program " << std::endl
    << "will try to detect markers on combined marker sheet" << std::endl;
}


static inline const bool waitForKey(const char key, const int delay) {
    return ((cv::waitKey(delay) % 256) == key);
}


double pointDistanceToLine(cv::Point point, cv::Point lineStart, cv::Point lineEnd) {

    double normalLength = std::hypot(lineEnd.x - lineStart.x, lineEnd.y - lineStart.y);
    double distance = (double) ((point.x - lineStart.x) * (lineEnd.y - lineStart.y) -
                                (point.y - lineStart.y) * (lineEnd.x - lineStart.x)) / normalLength;
    return distance;
}

std::vector<cv::Point> DouglasPeuckerApproximation(std::vector<cv::Point> contours, const double epsilon, bool closed) {
    double maxDist = 0.0;
    unsigned long  index = 0;
    unsigned long end = contours.size() - 1; // latest index of input array

    double d = 0.0;
    for (unsigned long  i = 1; i < end - 1; ++i) {
        d = pointDistanceToLine(contours[i], contours[0], contours[end]);
        if (d > maxDist) {
            index = i;
            maxDist = d;
        }
    }

    std::vector<cv::Point> result;

    if (epsilon < maxDist) {
        std::vector<cv::Point> subResult1, subResult2;

        std::vector<cv::Point>::const_iterator first = contours.begin();
        std::vector<cv::Point>::const_iterator last = contours.begin() + index - 1;

        std::vector<cv::Point> subVector1(first, last);

        first = last + 1;
        last = contours.end();
        std::vector<cv::Point> subVector2(first, last);

        subResult1 = DouglasPeuckerApproximation(subVector1, epsilon);
        subResult2 = DouglasPeuckerApproximation(subVector2, epsilon);

        subResult1.insert(subResult1.end(), subResult2.begin(), subResult2.end());
        result = subResult1;
    } else {
        result.clear();
        result.push_back(contours[0]);
        result.push_back(contours[end]);
    }

    return result;

}

static double vectorAngle(const cv::Point &pt1, const cv::Point &pt2, const cv::Point &pt0) {
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1 * dx2 + dy1 * dy2) / sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10);
}


cv::RNG rng(123424);


void putPoint(cv::Mat &im, const cv::Point point) {
    int thickness = -1;
    int lineType = 8;
    cv::Scalar color(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
    cv::circle(im, point, 1, color, thickness, lineType);
    //std::cout <<"point: " << point << std::endl;
}

void putRectangle(cv::Mat &im, const cv::Rect rectangle, const int thickness) {
    int lineType = 8;
    cv::Scalar color(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
    cv::rectangle(im, rectangle, color, thickness, lineType, 0);
}

void putText(cv::Mat &im, const std::string label, const cv::Rect rectangle, const int thickness,
             const cv::Scalar &color) {
    int fontface = cv::FONT_HERSHEY_SIMPLEX;
    double scale = 0.4;
    int baseline = 0;

    cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);

    cv::Point pt(rectangle.x + ((rectangle.width - text.width) / 2),
                 rectangle.y + ((rectangle.height + text.height) / 2));

    cv::putText(im, label, pt, fontface, scale, color, thickness, 8);
}

std::vector<Marker> findMarkers(cv::Mat &bwImg) {

    // Find contours
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(bwImg.clone(), contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));


    std::cout << contours.size() << "contours and " << hierarchy.size() << " hierarchy found" << std::endl;

    std::vector<Marker> result;

    std::vector<cv::Point> approx;


    for (int i = 0; i < contours.size(); i++) {

        std::vector<cv::Point> approx;
        // Approximate contour with accuracy proportional
        // to the contour perimeter
        cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true) * 0.02, true);

        // Skip small or non-convex objects
        if (std::fabs(cv::contourArea(contours[i])) < 100 || !cv::isContourConvex(approx))
            continue;

        unsigned long numOfVertices = approx.size();


        if (numOfVertices == 4) {
            // Number of vertices of polygonal curve

            // Get the cosines of all corners
            std::vector<double> cos;
            for (int j = 2; j < numOfVertices + 1; j++)
                cos.push_back(vectorAngle(approx[j % numOfVertices], approx[j - 2], approx[j - 1]));

            // Sort ascending the cosine values
            std::sort(cos.begin(), cos.end());

            // Get the lowest and the highest cosine
            double mincos = cos.front();
            double maxcos = cos.back();

            // Use the degrees obtained above and the number of vertices
            // to determine the shape of the contour
            if (mincos >= -0.1 && maxcos <= 0.3) {
                cv::Rect bRect = cv::boundingRect(contours[i]);

                if ((hierarchy[i][CH_IND_FIRST_CHILD]) > 0  // if contour has child
                    && (hierarchy[i][CH_IND_PARENT] < 0)  // and hs no parent
                    //) {//
                    && (bRect.width >= 100 && bRect.height >= 75)) { // and rect region is not too small

                    int k = hierarchy[i][CH_IND_FIRST_CHILD];
                    cv::Rect markerRect = cv::boundingRect(contours[k]);


                    if ((hierarchy[k][CH_IND_FIRST_CHILD] > 0)) { // inner edge of outer rectange
                        int l = hierarchy[k][CH_IND_FIRST_CHILD];


                        if (hierarchy[l][CH_IND_FIRST_CHILD] > 0) { // outer edge of inner rectangle
                            int m = hierarchy[l][CH_IND_FIRST_CHILD];

                            if (hierarchy[m][CH_IND_FIRST_CHILD] > 0) { // inner edge of inner rectangle

                                if (workOnRealMarkers) {
                                    int n = hierarchy[m][CH_IND_FIRST_CHILD];
                                    Marker mrkr;
                                    mrkr.area = markerRect;
                                    mrkr.number = 0;
                                    if (hierarchy[n][CH_IND_FIRST_CHILD] > 0) {
                                        int o = hierarchy[n][CH_IND_FIRST_CHILD];
                                        if(CH_NO_CONTOUR == o){
                                            mrkr.number = 0;
                                        }else if((hierarchy[o][CH_IND_NEXT] > 0) || (hierarchy[o][CH_IND_PREV] > 0)){
                                            mrkr.number = 2;
                                        } else  {
                                            mrkr.number = 1;
                                        }
                                    }
                                    result.push_back(mrkr);

                                } else {
                                    Marker mrkr;
                                    mrkr.area = bRect; // set marker area
                                    mrkr.number = 0;

                                    int n = hierarchy[m][CH_IND_FIRST_CHILD];

                                    if(CH_NO_CONTOUR == n){
                                        mrkr.number = 0;
                                    }else if ((hierarchy[n][CH_IND_NEXT] > 0) || (hierarchy[n][CH_IND_PREV] > 0)) {
                                        mrkr.number = 2;
                                    } else{
                                        mrkr.number = 1;
                                    }
                                    result.push_back(mrkr);
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    return result;
}

std::string getMarkerText(const unsigned int markerNumber){
    std::stringstream ss;
    ss << "MARKER-" << markerNumber;
    return ss.str();
}