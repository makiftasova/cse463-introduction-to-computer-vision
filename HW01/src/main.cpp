/**
 *
 *	Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 *	Student Number: 111044016
 *	CSE463 - Introduction to Computer Vision
 *	Homework 01 - Thresholding
 *
 */

#include <iostream>
#include <vector> // container for frames

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// EXIT CODES
//static const int EXIT_SUCCESS = 0;
static const int EXIT_FAIL_TO_OPEN_VIDEO_DEVICE = -10;


// DEVICE ID FOR WEB CAM(S)
static const int VIDEO_DEVICE_ID = 0;

// cv::waitKey waiting time in ms
static const int WAITKEY_WAIT_TIME = 50;

// SRINGS NAME
const std::string WINDOW_NAME_THRESHOLDING = "THRESHOLDING";
const std::string WINDOW_NAME_HISTOGRAM = "HISTOGRAM";
const std::string WINDOW_NAME_CAM_STREAM = "WEB CAM";

const std::string TRACKBAR_VALUE = "Threshold Value";


// GLOBALS
const int THRESHOLD_TYPE = 0; // binary thresholding
const int MAX_VALUE = 255;
const int MAX_BINARY_VALUE = 255;


const uchar PIXEL_MAX = 255;
const uchar PIXEL_MIN = 0;

/**
 * calculates binary threshold for given value
 * returns given image if given threshold value is greater than 255
 *
 * @param img image to calculate threshold
 * @param thresholdValue threshold value
 *
 * @return threshold image
 */
cv::Mat myThreshold(const cv::Mat &img, const unsigned short thresholdValue);

/**
 * Calculates histogram of a gray scale image
 *
 * @param gray scale img image to calculate histogram
 * @param maxPixelValue number of value range of single pixel
 *
 * @return Histogram image of given source image
 */
cv::Mat myCalcHist(const cv::Mat &img, const int maxPixelValue = 256);

/**
 * this will be called every time a mouse event occurs
 */
void mouseEventCallback(const int event, const int x, const int y, const int flags, void *args);

/**
 * this will be called every time trackbar on thresholding window is moved
 */
void thresholdCallback(int trackbarValue, void *inputsArray);

int main(int argc, char **argv) {

    cv::VideoCapture webCamStream(VIDEO_DEVICE_ID);

    if (!webCamStream.isOpened()) {
        std::cerr << "[ERR] Failed to open video device! device ID: " << VIDEO_DEVICE_ID << std::endl;
        return (EXIT_FAIL_TO_OPEN_VIDEO_DEVICE);
    }

    while (1) {


        std::cout << "[1] PART 1: Threasholding by Histogram\n" << "[2] PART 2: Threasholding by Trackbar\n"
        << "[3] Exit\n " << "Enter Your Choice >> ";
        unsigned short selection = 0;
        std::cin >> selection;


        switch (selection) {
            case 1: {
                cv::Mat rgbFrame; // create Matrix for storing frame
                cv::Mat grayFrame; // grayscale version of rgbFrame


                for (; ;) {
                    webCamStream >> rgbFrame;

                    cv::cvtColor(rgbFrame, grayFrame, CV_RGB2GRAY);
                    cv::Mat histImg = myCalcHist(grayFrame);

                    // create window for cam stream
                    cv::namedWindow(WINDOW_NAME_CAM_STREAM);
                    cv::imshow(WINDOW_NAME_CAM_STREAM, grayFrame);

                    // prepare histogram window
                    cv::namedWindow(WINDOW_NAME_HISTOGRAM);
                    cv::setMouseCallback(WINDOW_NAME_HISTOGRAM, mouseEventCallback, (void *) &grayFrame);


                    cv::imshow(WINDOW_NAME_HISTOGRAM, histImg);

                    if (cv::waitKey(WAITKEY_WAIT_TIME) >= 0) {
                        cv::destroyAllWindows();
                        break;
                    };
                }
            }
                break;

            case 2: {

                cv::namedWindow(WINDOW_NAME_THRESHOLDING); // create output window

                int thresholdValue = 0;

                cv::createTrackbar(TRACKBAR_VALUE,
                                   WINDOW_NAME_THRESHOLDING, &thresholdValue,
                                   MAX_VALUE, thresholdCallback);


                cv::Mat rgbFrame; // create Matrix for storing frame
                cv::Mat grayFrame; // grayscale version of rgbFrame
                cv::Mat thresholdResult; // result of tresholding

                // loop until device closes or user interrrupts
                for (; ;) {
                    webCamStream >> rgbFrame; // get next frame
                    cv::cvtColor(rgbFrame, grayFrame, CV_RGB2GRAY);

//                    cv::namedWindow(WINDOW_NAME_CAM_STREAM);
//                    cv::imshow(WINDOW_NAME_CAM_STREAM, grayFrame);

                    cv::threshold(grayFrame, thresholdResult, thresholdValue, MAX_BINARY_VALUE, THRESHOLD_TYPE);
                    // thresholdResult = myThreshold(grayFrame, thresholdValue);
                    cv::imshow(WINDOW_NAME_THRESHOLDING, thresholdResult);


                    // std::cout << "[INF] Current thresholding value: " << thresholdValue << std::endl;

                    // exit at any keypress
                    if (cv::waitKey(WAITKEY_WAIT_TIME) >= 0) {
                        cv::destroyAllWindows();
                        //cv::destroyWindow(WINDOW_NAME_THRESHOLDING);
                        break;
                    }
                }

            }
                break;

            case 3:
                webCamStream.release();
                cv::destroyAllWindows();
                std::cout << "[INF] Successful exit." << std::endl;
                return (EXIT_SUCCESS);

            default:
                std::cout << "[ERR] Illegal Choice!!!\n";
                break;

        };

    }
}


cv::Mat myThreshold(const cv::Mat &img, const unsigned short thresholdValue) {
    cv::Mat result = img.clone();

    if (thresholdValue < 256) {
        for (int i = 0; i < result.rows; i++) {
            for (int j = 0; j < result.cols; j++) {
                result.at<uchar>(i, j) = (result.at<uchar>(i, j) < thresholdValue ? PIXEL_MIN : PIXEL_MAX);
            }

        }
    }
    return result;
}

cv::Mat myCalcHist(const cv::Mat &img, const int maxPixelValue) {

    cv::Mat hist;       // histogram values

    // Initialize histogram matrix
    hist = cv::Mat::zeros(1, maxPixelValue, CV_32SC1);

    // Calculate the histogram of the image
    for (int i = 0; i < img.rows; i++) {
        for (int j = 0; j < img.cols; j++) {

            uchar val = img.at<uchar>(i, j);
            hist.at<int>(val) += 1;
        }
    }

    // obtain the maximum (peak) value of Histogram values
    // We need peak value for normalizing histogram image
    int peakVal = 0;
    for (int i = 0; i < maxPixelValue - 1; i++) {
        peakVal = hist.at<int>(i) > peakVal ? hist.at<int>(i) : peakVal; // god bless the ternary operator
    }

    cv::Mat histogramImage; //this will hold histogram image

    // Display histogram values in a histogramImage after normalizing them
    histogramImage = cv::Mat::ones(125, maxPixelValue, CV_8UC3);

    for (int i = 0, rows = histogramImage.rows; i < maxPixelValue - 1; i++) {
        cv::line(
                histogramImage,
                cv::Point(i, rows),
                cv::Point(i, rows - (hist.at<int>(i) * rows / peakVal)),
                cv::Scalar(250, 250, 250),
                1, 8, 0
        );
    }

    return histogramImage;
}


void mouseEventCallback(const int event, const int x, const int y, const int flags, void *args) {
    if (event == cv::EVENT_LBUTTONDOWN) { // if left click on window
        cv::Mat grayImage = ((cv::Mat *) args)->clone();
        cv::Mat thresholdImage;
        //cv::threshold(grayImage, thresholdImage, x, MAX_BINARY_VALUE, THRESHOLD_TYPE);
        thresholdImage = myThreshold(grayImage, x);
        cv::imshow(WINDOW_NAME_THRESHOLDING, thresholdImage);
        cv::waitKey(WAITKEY_WAIT_TIME);
    }

}

void thresholdCallback(int trackbarValue, void *inputsArray) {

    // std::cout << "[INF] Current thresholding value: " << thresholdValue << std::endl;

    // Intentionally Commented out

    // webCamStream >> rgbFrame;
    // cv::cvtColor(rgbFrame, grayFrame, CV_RGB2GRAY);
    // cv::threshold( grayFrame, thresholdResult, trackbarValue, MAX_BINARY_VALUE, THRESHOLD_TYPE );

    // cv::imshow(WINDOW_NAME_THRESHOLDING, thresholdResult);
    // std::cout << "[INF] Current thresholding value: " << trackbarValue << std::endl;
}